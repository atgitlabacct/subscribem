require_dependency "subscribem/application_controller"

module Subscribem
  class Account::SessionsController < ApplicationController
    def new
      @user = User.new
    end

    def create
      if env["warden"].authenticate(scope: :user)
        logger.info("Session created")
        redirect_to root_path, notice: "You are now signed in."
      else
        logger.error("Failed to create session")
        @user = User.new
        flash[:error] = "Invalid email or password."
        render action: "new"
      end
    end
  end
end
